import {SAVE_FUEL_SAVINGS, CALCULATE_FUEL_SAVINGS, SET_CURRENT_TABS, ADD_NEW_FEEDBACK} from '../constants/actionTypes';
import calculator from '../utils/fuelSavingsCalculator';
import objectAssign from 'object-assign';
import initialState from './initialState';

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function fuelSavingsReducer(state = initialState.fuelSavings, action) {

  let newState, newList;
  switch (action.type) {

    case ADD_NEW_FEEDBACK:
      newState = objectAssign({}, state);
      newList = [].concat(newState.feedbackList);
      newList.push(action.data);
      newState.feedbackList = newList;
      return newState;

    case SET_CURRENT_TABS:
      newState = {...state, currentTabs: action.tab};
      return newState;

    case SAVE_FUEL_SAVINGS:
      // For this example, just simulating a save by changing date modified.
      // In a real app using Redux, you might use redux-thunk and handle the async call in fuelSavingsActions.js
      return objectAssign({}, state, {dateModified: action.dateModified});

    case CALCULATE_FUEL_SAVINGS:
      newState = objectAssign({}, state);
      newState[action.fieldName] = action.value;
      newState.necessaryDataIsProvidedToCalculateSavings = calculator().necessaryDataIsProvidedToCalculateSavings(newState);
      newState.dateModified = action.dateModified;

      if (newState.necessaryDataIsProvidedToCalculateSavings) {
        newState.savings = calculator().calculateSavings(newState);
      }

      return newState;

    default:
      return state;
  }
}
