import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {bindActionCreators} from 'redux';
import * as tabsActions from '../actions/tabsActions';
import * as fuelActions from '../actions/fuelSavingsActions';
import FuelSavingsForm from '../components/FuelSavingsForm';
import FeedbackForm from '../components/FeedbackForms/FeedbackForm';
import FeedbackList from '../components/FeedbackForms/FeedbackList';

class HomePage extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.handleOnClick = this.handleOnClick.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.renderTabsContent = this.renderTabsContent.bind(this);
  }

  handleOnClick(e) {
    e.preventDefault();
    const dataTag = parseInt(e.target.getAttribute('data-tag'));
    this.props.tabsActions.setCurrentTabs(dataTag);
  }

  renderTabs() {
    const tabs = [
      {name: 'Tab 1', index: 1},
      {name: 'Tab 2', index: 2},
      {name: 'Tab 3', index: 3},
    ];
    const currentTabs = this.props.fuelSavings.currentTabs;
    return (
      <div>
        <ul className="nav nav-tabs" role="tablist">
          {tabs.map((i) => {
            let sltyle = (i.index === currentTabs) ? 'active' : '';
            return (
              <li role="presentation" className={sltyle} key={i.index}>
                <a data-tag={i.index} href="#" onClick={this.handleOnClick}>{i.name}</a>
              </li>
            );
          })}
        </ul>
        <div className="box">
          {this.renderTabsContent(currentTabs)}
        </div>
      </div>

    );
  }

  renderTabsContent(currentTabs) {

    switch (currentTabs) {
      case 1:
        return (
          <FuelSavingsForm
            saveFuelSavings={this.props.fuelActions.saveFuelSavings}
            calculateFuelSavings={this.props.fuelActions.calculateFuelSavings}
            fuelSavings={this.props.fuelSavings}
          />
        );
      case 2:
        return (
          <FeedbackForm
            feedbackForm={this.props.feedbackForm}
            onSubmit={this.props.tabsActions.addNewFeedback}
          />
        );

      case 3:
        return (
          <FeedbackList
            feedbackList={this.props.fuelSavings.feedbackList}
          />
        );

      default: return;
    }
  }

  render() {
    return (
      <div>
        <h1>React Slingshot</h1>

        <h2 className="container-h2">Get Started</h2>
        <ol>
          <li>Review the <Link to="fuel-savings">demo app</Link></li>
          <li>Remove the demo and start coding: npm run remove-demo</li>
        </ol>

        {this.renderTabs()}

      </div>
    );
  }
}

HomePage.propTypes = {
  tabsActions: PropTypes.object.isRequired,
  fuelActions: PropTypes.object.isRequired,
  fuelSavings: PropTypes.object.isRequired,
  feedbackForm: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    fuelSavings: state.fuelSavings,
    feedbackForm: state.form
  };
}

function mapDispatchToProps(dispatch) {
  return {
    tabsActions: bindActionCreators(tabsActions, dispatch),
    fuelActions: bindActionCreators(fuelActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
