export default (values) => {
  const errors = {};
  if (!values.from) {
    errors.from = 'Required';
  } else if (values.from.length > 25) {
    errors.from = 'Must be 25 characters or less';
  } else if (values.from.length < 5) {
    errors.from = 'Must be at least 5 characters';
  }

  return errors;
};
