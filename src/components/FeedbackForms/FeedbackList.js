import React, {PropTypes} from 'react';

export default class FeedbackList extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.renderList = this.renderList.bind(this);
  }

  renderList() {
    let feedbackList = this.props.feedbackList;
    return feedbackList.map((i, ind) => {
      return (
        <div key={ind}>
          <h3>{i.from}</h3>
          <p>{i.feedback}</p>
          <hr/>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        <h2 className="container-h2">Feedback</h2>
        {this.renderList()}
      </div>
    );
  }
}

FeedbackList.propTypes = {
  feedbackList: PropTypes.array.isRequired
};
