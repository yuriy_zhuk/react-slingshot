import React, {PropTypes} from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';

class FeedbackForm extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit} className="navbar-form feedbeck-form">
        <h2 className="container-h2">Feedback Form</h2>

        <Field
          name="from"
          type="text"
          component={renderField}
          label="From"
          placeholder="Input feedback"
        />
        <br/>
        <Field
          name="feedback"
          type="textarea"
          component={renderTextarea}
          label="Feedback"
          placeholder="Input feedback"
        />
        <br/>
        <div>
          <button type="submit" className="btn btn-default">Send</button>
        </div>
      </form>
    );

  }
}

// eslint-disable-next-line
const renderTextarea = ({input, label, placeholder, meta: {touched, error, warning}}) => {

  return (
    <div>
      <label>{label}</label>
      <textarea {...input}
                rows="5"
                className="form-control"
                placeholder={placeholder}
                style={{width: '100%'}}
      />
      {touched && ((error && <span className="feedbeck-error">{error}</span>)
      || (warning && <span>{warning}</span>))}
    </div>
  );
};

// eslint-disable-next-line
const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input}
             placeholder={label}
             type={type}
             className="form-control"
             style={{width: '100%'}}
      />
      {touched && ((error && <span className="feedbeck-error">{error}</span>)
      || (warning && <span>{warning}</span>))}
    </div>
  </div>
);

FeedbackForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  feedbackForm: PropTypes.object.isRequired
};

export default reduxForm({
  form: 'feedbackForm',
  validate,
})(FeedbackForm);

