import * as types from '../constants/actionTypes';

export function setCurrentTabs(tab) {
  return function (dispatch) {
    return dispatch({
      type: types.SET_CURRENT_TABS,
      tab
    });
  };
}

export function addNewFeedback(data) {
  return {
    type: types.ADD_NEW_FEEDBACK,
    data
  };
}
